//
//  TYModel.h
//  TYModel
//
//  Created by 夏伟 on 16/9/21.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface TYModel : MTLModel <MTLJSONSerializing>

// Dictionary -> Model
+ (instancetype)modelFromDictionary:(NSDictionary *)dict;
// Array -> Model Array
+ (NSArray *)arrayOfModelsFromArray:(NSArray *)array;

// Model -> Dictionary
- (NSDictionary *)transfromToDictionary;
// Model Array -> Array
+ (NSArray *)transfromToArray:(NSArray *)array;

@end
