//
//  TYModel.m
//  TYModel
//
//  Created by 夏伟 on 16/9/21.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYModel.h"

@implementation TYModel

#pragma mark - Mantle
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSLog(@"You must override %@ in a subclass", NSStringFromSelector(_cmd));
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

#pragma mark - Parse
+ (instancetype)modelFromDictionary:(NSDictionary *)dict {
    NSError *error;
    id model = [MTLJSONAdapter modelOfClass:[self class] fromJSONDictionary:dict error:&error];

    if (error) {
        NSLog(@"Couldn't convert NSDictionary JSON to Model: %@: %@", NSStringFromClass(self), error);
        return nil;
    }
    return model;
}

+ (NSArray *)arrayOfModelsFromArray:(NSArray *)array {
    NSError *error;
    NSArray *arrayOfModels = [MTLJSONAdapter modelsOfClass:[self class] fromJSONArray:array error:&error];

    if (error) {
        NSLog(@"Couldn't convert NSArray JSON to %@ Models: %@", NSStringFromClass(self), error);
        return nil;
    }
    return arrayOfModels;
}

- (NSDictionary *)transfromToDictionary {
    return [MTLJSONAdapter JSONDictionaryFromModel:self error:nil];
}

+ (NSArray *)transfromToArray:(NSArray *)array {
    return [MTLJSONAdapter JSONArrayFromModels:array error:nil];
}

@end
