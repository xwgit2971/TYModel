Pod::Spec.new do |s|
  s.name = 'TYModel'
  s.version = '0.0.1'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'TYModel'
  s.homepage = 'https://gitlab.com/xwgit2971/TYModel'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYModel.git', :tag => s.version }
  s.source_files = 'TYModel/*.{h,m}'
  s.framework = 'Foundation'
  s.requires_arc = true
  s.dependency  'Mantle', '~> 2.1.0'
end
